from flask import Flask, escape, request
from kubemq.commandquery.channel import Channel
from kubemq.commandquery.channel_parameters import ChannelParameters
from kubemq.commandquery.request import Request
from kubemq.commandquery.request_type import RequestType



app = Flask(__name__)

@app.route('/')
def hello():
    send_query_request()

    return '{}'.format(send_query_request())

def create_request_channel_parameters(request_type):
    return ChannelParameters(
        channel_name="MyTestChannelName",
        client_id="CommandQueryChannel",
        timeout=111000,
        cache_key="",
        cache_ttl=0,
        request_type=request_type,
    )


def send_query_request():
    request_channel_parameters = create_request_channel_parameters(RequestType.Query)
    request_channel = Channel(channel_parameters=request_channel_parameters, kubemq_address="localhost:50000")

    request = Request(
        metadata="CommandQueryChannel",
        body="getRand".encode('UTF-8')
    )

    response = request_channel.send_request(request)
    return response.body
